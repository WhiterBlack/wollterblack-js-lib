const merge = require('lodash/merge');

function encrypt(text) {
    let {algorithm, secret} = config.get('cryptoError');
    let cipher = crypto.createCipher(algorithm, secret);
    let crypted = cipher.update(text, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
}


module.exports = (config) => {
    config = merge({
        logger:console,
        requiredEncrypt:process.env.NODE_ENV === 'production',
        encryptToAuthUser:false,
        encryptParams:{
            algorithm:'aes-256-ctr',
            secret:'tuturu'
        }
    },config);

    return (err,req,res,next)=> {
        config.logger.error(err);
        let errMessage = JSON.stringify({
            reqId: req.requestId,
            message: err.toString(),
            stack: err.stack ? err.stack.toString() : null
        }, null, 4);
        if(config.requiredEncrypt && (config.encryptToAuthUser && !req.user)) {
            errMessage = encrypt(errMessage,config.encryptParams);
        }
        res.status(500).send({
            status: false,
            serverError: true,
            message: errMessage,
            errData:process.env.NODE_ENV === 'development'?JSON.parse(errMessage):null
        })
    }
}