const {performance} = require('perf_hooks');

module.exports = async (req, res) => {
    let time = performance.now();

    process.nextTick(() => {
        let perf = performance.now();
        res.send({
            tickTime: perf - time,
            reqPerf: perf - req.perf,
            reqId: req.requestId,
            status: true,
        })
    })
};