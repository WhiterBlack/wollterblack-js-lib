const lodash = require('lodash');
const express = require("express");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const busboy = require("connect-busboy");
const {performance} = require('perf_hooks');
const uuid = require('uuid').v4;
const SessionStorage = require('./middelware/SessionStorage');
const morgan = require('morgan');

class ExpressApplication {
    constructor(config) {
        this.configuration = lodash.merge({
            logger:console,
            redis:{},
            sessionStorage: {
                secret:'secret'
            }
        },config);
        this.app = express();
        this.configurate();
        this.setRequestId();
        this.setLogger();
        this.setParsers();
        this.setSessionStorage();
        this.setAuth();
        this.setBaseRoute();
    }

    configurate() {
        this.app.set("x-powered-by", false);
        this.app.set("trust proxy", true);
    }

    setRequestId() {
        let _reqId = 0;
        this.app.use((req,res,next)=> {
            req.perf = performance.now();
            req.requestId = process.env.NODE_ENV === 'development' ? ++_reqId : uuid();
            next();
        })
    }

    setParsers() {
        this.app.use(bodyParser.json({}));
        this.app.use(
            bodyParser.urlencoded({
                extended: true,
            })
        );
        this.app.use(cookieParser());
        this.app.use(busboy());
    }

    setSessionStorage() {
        SessionStorage(this.app,{
            redis:this.configuration.redis,
            sessionStorage:this.configuration.sessionStorage
        })
    }

    setAuth() {

    }

    setBaseRoute() {
        this.app.get('/',(req,res)=> {
            return res.send('Переопределите setBaseRoute()!');
        })
    }

    setLogger() {
        this.app.use(morgan(':method :url :status :res[content-length] - :response-time ms'));
    }

     getApp() {
        return this.app;
    }
}

module.exports = ExpressApplication;