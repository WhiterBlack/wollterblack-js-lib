const passport = require('passport');
const Strategy = require('passport-local').Strategy;


class Auth {
    getModel() {
        return this.model;
    }

    constructor(config = {logger:console}) {
        this.model = config.model;
        this.logger = config.logger;
    }

    async getStrategy(username, password, done) {
        let Model = this.getModel();
        try {
            let user = await Model.findOne({username});
            if (!user) {
                this.logger.warn(`Пользователь ${username} не найден`);
                done(null, false);
                return;
            }
            if (!user.checkPassword(password)) {
                this.logger.warn(`Попытка авторизации под пользователем ${username} под неправильным паролем`);
                done(null, false);
                return;
            }
            done(null, user);

        } catch (e) {
            this.logger.error(e);
            done(e, null);
        }
    }

    serializeUser(user,cb) {
        cb(null, user._id);
    }

    async deserializeUser(id,cb) {
        let Model = this.getModel();
        let user = await Model.findById(id);
        cb(null, user);
    }

    connectToApp(app) {
        passport.use('local', new Strategy(this.getStrategy.bind(this)));
        passport.serializeUser(this.serializeUser.bind(this));
        passport.deserializeUser(this.deserializeUser.bind(this));
        app.use(passport.initialize());
        app.use(passport.session());
        app.set('passport', passport);
        app.use((req,res,next)=> {
            req.passport = passport;
            next();
        })
    }
}

module.exports = Auth;