const session = require('express-session');
let RedisStore = require('connect-redis')(session);
const redis = require('redis');

module.exports = function (app,config) {
    const redisClient = config.redisClient || redis.createClient(config.redis);
    app.use(
        session({
            store: new RedisStore({
                client: redisClient,
                logErrors: (err) => config.logger.error(err.message)
            }),
            secret: config.sessionStorage.secret,
            resave: false,
            saveUninitialized: false,
            name: config.sessionStorage.cookieName
        })
    );
};
