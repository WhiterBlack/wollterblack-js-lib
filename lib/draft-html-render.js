const merge = require('lodash/merge');

const defaultBlockRender = (block) => {
    return `<p>${block.text}</p>`;
}

const defaultConfig = {
    customBlocks: {
        'header-tree': defaultBlockRender,
        'unstyled': defaultBlockRender,
    }
}

async function render(src, config = {}) {
    if (!src) return null;
    config = merge(config, defaultConfig);
    let html = [];
    for (let block of src.blocks) {
        if (config.customBlocks[block.type]) {
            html.push(await config.customBlocks[block.type](block));
            continue;
        }
        console.warn('Нераспознаный блок данных: ', block);

    }
    return html.join('');
}

module.exports = render;