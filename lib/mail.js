const pug = require('pug');
const path = require('path');
const nodeMailer = require('nodemailer');

class SendMail {
    constructor(config = {logger:console,testMode:true}) {
        this.config = config;
    }

    async sendMail(to, subject, html, props) {
        let mailSettings = this.config;
        if (config.testMode) {
            let account = await nodeMailer.createTestAccount();
            mailSettings = {
                ...mailSettings,
                host: 'smtp.ethereal.email',
                port: 587,
                secure: false, // true for 465, false for other ports
                auth: {
                    user: account.user, // generated ethereal user
                    pass: account.pass // generated ethereal password
                }
            }
        }
        let emailTransport = nodeMailer.createTransport(mailSettings);
        let mailOptions = {
            from: mailSettings.from, // sender address
            to,
            subject,
            html,
            ...props
        };

        try {
            let sendResult = await emailTransport.sendMail(mailOptions);
            if (config.testMode) {
                logger.info('Preview URL: %s', nodeMailer.getTestMessageUrl(sendResult));
            }
            logger.info('email send!', sendResult.accepted, sendResult.response);
            logger.debug(sendResult);
        } catch (e) {
            logger.error('Email error', e.message);
            throw e;
        }
    }

    async sendTemplate(templateName, to = '', subject = '', data = {}, props = {}) {
        let template = path.join(`${templateName}.pug`);
        let html = pug.renderFile(template, data);
        await this.sendMail(to, subject, html, props);
    }
}

module.exports = SendMail;