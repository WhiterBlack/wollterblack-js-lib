/**
 * Created by WhiterBlack on 29.07.2016.
 */
const mongoose = require('lib/mongoose');

const log = require('./logger')(module);
const config = require('./config');

mongoose.Promise = global.Promise;
mongoose.connect(
    config.get('database:url'),
    Object.assign(
        {
            useNewUrlParser: true
        },
        config.get('database:_params')
    )
);
log.info(`Mongoose version: ${mongoose.version}`);
const db = mongoose.connection;

let ConAttempts = 0;
db.once('open', function () {
    ConAttempts = 0;
    let admin = new mongoose.mongo.Admin(mongoose.connection.db);
    admin.buildInfo(function (err, info) {
        log.info(`Mongodb connected: Version: ${info.version}`);
        log.debug(info);
    });
});

db.on('error', function (err) {
    log.error('connection error:', err.message);
    ConAttempts++;
    if (config.get('database:reconnect:enabled')) {
        setTimeout(function Reconnect() {
            if (
                ConAttempts < config.get('database:reconnect:Attempts') ||
                config.get('database:reconnect:Attempts') === 0
            ) {
                mongoose.connect(
                    config.get('database:url', config.get('database:params'))
                );
            } else {
                ErrorAndClose(err);
            }
        }, config.get('db:reconnect:interval'));
    } else {
        ErrorAndClose(err);
    }
});

function ErrorAndClose(err) {
    log.error(`connection error: ${err.message}. Attempts: ${ConAttempts}`);
    process.exit(-1);
}

module.exports = mongoose;
