/**
 * Created by WhiterBlack on 31.03.2016.
 */
const https = require('https');
const logger = require('./logger')(module);

module.exports = function (secret) {
    if (!secret) {
        secret = require('./config').get('recaptcha:secret');
    }
    return (key) => new Promise((done, reject) => {
        https.get("https://www.google.com/recaptcha/api/siteverify?secret=" + secret + "&response=" + key, function (res) {
            let data = "";
            res.on('data', function (chunk) {
                data += chunk.toString();
            });
            res.on('end', function () {
                try {
                    const parsedData = JSON.parse(data);
                    //console.log(parsedData);
                    logger.debug(null, parsedData.success);
                    done(parsedData.success);
                } catch (e) {
                    reject();
                    logger.debug(e, false);
                }
            });
        });
    })
};

