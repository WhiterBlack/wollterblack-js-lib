const http = require("http");

class CreateServer {
    constructor(app,config) {
        this.config = config;
        this.logger = config.logger || console;
        this.server = http.createServer(app);
    }

    listen() {
        this.server.listen(this.getPort(), this.getHost());
        this.server.on("listening",this.onListen.bind(this));
        this.server.on('error',this.onError.bind(this));
    }

    getPort() {
        let port = process.env.PORT || process.env.APP_PORT || this.config.port;
        if(!port) throw new Error('Порт не указан');
        return port;
    }

    getHost() {
        return process.env.HOST || process.env.APP_HOST || this.config.host || '0.0.0.0';
    }

    onError(error) {
        if (error.syscall !== "listen") {
            throw error;
        }
        const port = this.getPort();
        let bind =
            typeof port === "string"
                ? "Pipe " + port
                : "Port " + port;

        switch (error.code) {
            case "EACCES":
                this.logger.error(
                    bind +
                    " Не хватает системных прав (укажите порт выше 1024 или запустите приложение от имени root (Если у вас unix based система))"
                );
                process.exit(1);
                break;
            case "EADDRINUSE":
                this.logger.error(`Порт ${port} уже используется`);
                process.exit(1);
                break;
            default:
                throw error;
        }
    }

    onListen() {
        let addr = this.server.address();
        let bind = typeof addr === "string" ? "pipe " + addr : "port " + addr.port;
        this.logger.info("Listening on " + bind);
        this.logger.info(`Откройте http://localhost:${addr.port} в браузере`);
    }
}

module.exports = CreateServer;
