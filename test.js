const Server = require('./Server/CreateServer');
const Express = require('./CreateExpressApplication');


class MyApp extends Express {
    setBaseRoute() {
        this.app.get('/',(req,res)=> {
            res.send('PASSED!');
        })
    }
}

let app = new MyApp({
    Auth:false
});

let server = new Server(app.getApp(),{
    port:3333
});

server.listen();